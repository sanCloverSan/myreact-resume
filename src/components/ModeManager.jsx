import React, { Component } from "react";
import { Container, Button } from "reactstrap";
import LandingComponent from "./landing/Landing.jsx";
// import AboutMeComponent from "./aboutme/AboutMe.jsx";
import MainTerminalComponent from "./geekmode/main_terminal/MainTerminal.jsx";

class ModeManager extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: "normal",
    };
  }

  /**
   * Method to handle the mode change
   * @param {Event} e
   */
  handleModeChange = (e) => {
    e.preventDefault();
    let currMode = this.state.mode;
    let buttonChild = document.getElementById("changeModeBtn");
    if (buttonChild) {
      if (currMode === "normal") {
        this.setState({ mode: "geek" });
        buttonChild.classList.remove("modeButtonLeft");
        buttonChild.classList.add("modeButtonRight");
      } else {
        this.setState({ mode: "normal" });
        buttonChild.classList.remove("modeButtonRight");
        buttonChild.classList.add("modeButtonLeft");
      }
    }
  };

  handleOnScroll = (e) => {
    let top = window.scrollY; 
    // let left = window.scrollX;
    // let screenHeight = window.screen.availHeight ;
    console.log(top)
  };

  render() {
    let currMode = this.state.mode;

    window.addEventListener("scroll", this.handleOnScroll, false);

    return (
      <Container fluid className="m-0 p-0">
        <Container className="modeContainer m-0">
          <p className="m-0 mb-1 ml-2">Mode</p>
          <Container
            className=" m-0 p-0 modeButtonContainer"
            onClick={this.handleModeChange}
          >
            <Button id="changeModeBtn" className="modeButtonLeft bg-black">
              {currMode === "normal" ? "Normal" : "Geek"}
            </Button>
          </Container>
        </Container>
        {/* <Container className="">Navigation</Container> */}
        <Container fluid className=" m-0 p-0 modeContent">
          {currMode === "normal" ? (
            <React.Fragment>
              <LandingComponent />
              {/* <AboutMeComponent /> */}
            </React.Fragment>
          ) : (
            <React.Fragment>
              <MainTerminalComponent/>
            </React.Fragment>
          )}
        </Container>
      </Container>
    );
  }
}

export default ModeManager;
