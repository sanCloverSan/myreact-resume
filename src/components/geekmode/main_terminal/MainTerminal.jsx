// import React, { Component, useState, useEffect } from "react";
import React, { Component} from "react";
import { Container } from "reactstrap";
import "./MainTerminal.css";

class MainTerminalComponent extends Component {
  terminalInputFocusListener = (e) => {
    let inputelem = document.getElementById("command_input");
    inputelem.focus();
  };

  render() {
    return (
      <Container
        fluid
        id=""
        className="col align-items-center  p-4 h4 col-md-6 h-100vh"
      >
        <Container
          fluid
          className="mt-5 p-0 m-0 main-terminal-container"
          onClick={() => {
            this.terminalInputFocusListener();
          }}
        >
          <div className="bg-black p-4 rounded-top">Terminal</div>
          <Container className="pt-4 pb-4  row bg-blue-normal m-0">
            <p className="col col-sm-4">cpatrick@myresume : </p>
            <p id="pwd" className="col col-sm-3  text-danger">
              ~/home/cpatrick
            </p>
            <input
              id="command_input"
              name="command_input"
              type="text"
              maxLength="32"
              className="col col-sm-rest p-0 pb-3 pl-2 text-white left"
              autoFocus
            />
          </Container>
          <Container className="p-3 text-center ">
            <span className="text-primary text-1rem">Enter command</span>
            <span className="text-1rem"> --help </span>
            <span className="text-primary text-1rem">
              for more information.
            </span>
          </Container>
        </Container>
      </Container>
    );
  }
}

export default MainTerminalComponent;
