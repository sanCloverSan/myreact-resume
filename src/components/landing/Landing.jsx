import React, { Component } from "react";
import { Container } from "reactstrap";
import Particle from "../animation/Particle";
import LandingAnimationCanvas from "./LandingAnimationCanvas";
// import profileImage from "../../public/images/profile-image";
import "./Landing.css";

const initParticles = () => {
  let particlesArrays = [];

  let numberOfParticles = (window.innerWidth * window.innerHeight) / 9000;

  for (let i = 0; i < numberOfParticles; i++) {
    let size = Math.random() * 5 + 1;
    let x =
      Math.random() * (window.innerWidth - size * 2) - size * 2 + size * 2;
    let y =
      Math.random() * (window.innerHeight - size * 2) - size * 2 + size * 2;
    let directionX = Math.random() * 5 - 2.5;
    let directionY = Math.random() * 5 - 2.5;
    let color = "#FFFFFF";
    particlesArrays.push(
      new Particle(x, y, directionX, directionY, size, color)
    );
  }
  return particlesArrays;
};

const connect = (particlesArrays, canvas, ctx) => {
  for (let a = 0; a < particlesArrays.length; a++) {
    for (let b = a; b < particlesArrays.length; b++) {
      let distance =
        (particlesArrays[a].x - particlesArrays[b].x) *
          (particlesArrays[a].x - particlesArrays[b].x) +
        (particlesArrays[a].y - particlesArrays[b].y) *
          (particlesArrays[a].y - particlesArrays[b].y);

      if (distance < (canvas.width / 7) * (canvas.height / 7)) {
        ctx.strokeStyle = "rgba(255, 255, 255, 0.443) ";
        ctx.beginPath();
        ctx.moveTo(particlesArrays[a].x, particlesArrays[a].y);
        ctx.lineTo(particlesArrays[b].x, particlesArrays[b].y);
        ctx.stroke();
      }
    }
  }
};

class LandingComponent extends Component {
  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
  }

  render() {
    const particles = initParticles();
    return (
      <Container fluid className="m-0 p-0">
        <Container fluid className="landingBackground" />
        <Container className="containerCenter round">
          <img
            src={"images/profile_image.png"}
            alt="a geek"
            className="profilePhoto"
          />
        </Container>

        <h2 className="welcomeText">
          Hi, my name is <strong>Christian Patrick</strong>
        </h2>
        
        <LandingAnimationCanvas
          particles={particles}
          connect={(particlesArrays, canvas, ctx) => {
            connect(particlesArrays, canvas, ctx);
          }}
        />
      </Container>
    );
  }
}

export default LandingComponent;
