import React from "react";
import useCanvas from "../UseCanvas";

import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  canvasBackground: {
    width:"100vw",
    position: "absolute",
    mixBlendMode: "overlay",
    zIndex: "1",
    padding: 0,
    margin: 0,
    top: 0,
  },
});

const LandingAnimationCanvas = (props) => {
  const classes = useStyles();
  const { particles, connect } = props;
  const canvasRef = useCanvas(particles, connect);

  return (
    <canvas ref={canvasRef} {...props} className={classes.canvasBackground} />
  );
};

export default LandingAnimationCanvas;
