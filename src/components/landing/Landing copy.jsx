import React from "react";
import { createUseStyles } from "react-jss";
import { Container, CardImg } from "reactstrap";
import Particle from "../animation/Particle";
import LandingAnimationCanvas from "./LandingAnimationCanvas";
// import profileImage from "../../public/images/profile-image";

const useStyles = createUseStyles({
  profilePhoto: {
    position: "absolute",
    left: "42.5%",
    top: "25%",
    width: "15%",
    borderRadius: "50%",
    boxShadow: "2px 10px 10px black",
    zIndex: 2,
  },
  landingBackground: {
    backgroundImage:
      "linear-gradient(180deg, #FFFFFF 0%, rgba(196, 196, 196, 0) 100%)",
    height: "100vh",
    width:"100vw",
    mixBlendMode: "overlay",
    zIndex: "0",
    padding: 0,
    margin: 0,
  },
  welcomeText: {
    position: "absolute",
    top: "65%",
    textAlign: "center",
    width: "100%",
    "&:before": {
      content: '""',
      display: "block",
      width: "150px",
      height: "2px",
      background: "#E7E7DE",
      left: "27%",
      top: "50%",
      position: "absolute",
    },
    "&:after": {
      content: '""',
      display: "block",
      width: "150px",
      height: "2px",
      background: "#E7E7DE",
      left: "65%",
      top: "50%",
      position: "absolute",
    },
  },
  textStripe: {
    position: "absolute",
    width: "136px",
    height: "0px",
    border: "2px solid #E7E7DE",
  },
});

const initParticles = () => {
  let particlesArrays = [];

  let numberOfParticles = (window.innerWidth * window.innerHeight) / 9000;

  for (let i = 0; i < numberOfParticles; i++) {
    let size = Math.random() * 5 + 1;
    let x =
      Math.random() * (window.innerWidth - size * 2) - size * 2 + size * 2;
    let y =
      Math.random() * (window.innerHeight - size * 2) - size * 2 + size * 2;
    let directionX = Math.random() * 5 - 2.5;
    let directionY = Math.random() * 5 - 2.5;
    let color = "#FFFFFF";
    particlesArrays.push(
      new Particle(x, y, directionX, directionY, size, color)
    );
  }
  return particlesArrays;
};

const connect = (particlesArrays, canvas, ctx) => {
  for (let a = 0; a < particlesArrays.length; a++) {
    for (let b = a; b < particlesArrays.length; b++) {
      let distance =
        (particlesArrays[a].x - particlesArrays[b].x) *
          (particlesArrays[a].x - particlesArrays[b].x) +
        (particlesArrays[a].y - particlesArrays[b].y) *
          (particlesArrays[a].y - particlesArrays[b].y);

      if (distance < (canvas.width / 7) * (canvas.height / 7)) {
        ctx.strokeStyle = "rgba(255, 255, 255, 0.443) ";
        ctx.beginPath();
        ctx.moveTo(particlesArrays[a].x, particlesArrays[a].y);
        ctx.lineTo(particlesArrays[b].x, particlesArrays[b].y);
        ctx.stroke();
      }
    }
  }
};

const LandingComponent = () => {
  const classes = useStyles();
  const particles = initParticles();
  return (
    <Container fluid className="m-0 p-0">
      <Container fluid className={classes.landingBackground} />

      <CardImg
        width="50%"
        src={"images/profile_image.png"}
        alt="Card image cap"
        className={classes.profilePhoto}
      />
      <h2 className={classes.welcomeText}>
        Hi, my name is <strong>Christian Patrick</strong>
      </h2>
      <LandingAnimationCanvas particles={particles} connect={(particlesArrays, canvas, ctx)=>{connect(particlesArrays, canvas, ctx)}} />
    </Container>
  );
};

export default LandingComponent;
