import { useRef, useEffect } from "react";

const useCanvas = (particles, connect) => {
  const canvasRef = useRef(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    const context = canvas.getContext("2d");

    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    let animationFrameId;

    //Our draw came here
    const render = () => {
      context.clearRect(0, 0, canvas.width, canvas.height);

      for (let i = 0; i < particles.length; i++) {
        let particle = particles[i];
        particle.update(canvas, context);
      }
      connect(particles, canvas, context);
      animationFrameId = window.requestAnimationFrame(render);
    };

    render();

    return () => {
      window.cancelAnimationFrame(animationFrameId);
    };
  }, [particles, connect]);

  return canvasRef;
};

export default useCanvas;
