// import React, { Component, useState, useEffect } from "react";
import React, { Component} from "react";
import { Container } from "reactstrap";

class AboutMeComponent extends Component {
  render() {
    return (
      <Container fluid className="m-0 p-0 h-100vh text-center">
        <Container>
          <cite>I love to Code Everyday</cite> - Nobody Ever
        </Container>

        <Container fluid className="">
          <p>Coding</p>
          <p>Sleeping ^ Coffee</p>
          <p>CTF-ing</p>
          <p>Drawing</p>
          <p>Gaming</p>
        </Container>

        <Container>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac
          mauris eget tellus consectetur placerat. Morbi maximus faucibus orci
          eu feugiat. Duis sit amet lorem at ex tincidunt aliquet bibendum
          pretium ante. Sed a lobortis diam. Nullam non tincidunt sapien.
          Praesent at posuere ante. Curabitur non purus ut sapien eleifend
          dictum quis non nisi.
        </Container>
      </Container>
    );
  }
}

export default AboutMeComponent;
