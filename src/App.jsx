import "./App.css";
import React from "react";
import { Container } from "reactstrap";
import { createUseStyles } from "react-jss";

import ModeManager from "./components/ModeManager.jsx";

const useStyles = createUseStyles({
  mainComp: {
    backgroundColor: "#0f3057",
    color: "#e7e7de",
    height: ["100vh", "!important"],
    width:["100%", "!important"],
    margin: "0px",
    padding: "0px",
    fontFamily: "Roboto Mono, monospace",
  },
});

function App() {
  let currMode = "normal";
  const classes = useStyles({ currMode: currMode });

  return (
    <Container fluid className={classes.mainComp}>
      <ModeManager />
    </Container>
  );
}

export default App;
